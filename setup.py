# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(name='YPblog',
      version='0.1',
      description=u'YP blog',
      long_description='',
      keywords='',
      author='ewgenius',
      author_email='ewgeniux@gmail.com',
      url='',
      package_dir={'': '.'},
      packages=find_packages('.'),
      include_package_data=True,
      zip_safe=False,
      install_requires=['distribute',
                        'Django',
                        'psycopg2',
                        'South',
      ])
