<uwsgi>
  <socket>${uwsgiconfig:socket}</socket>
<!-- <http>localhost:8000</http>  -->
  <chmod-socket>666</chmod-socket>
  <file>${uwsgiconfig:file}</file>
  <processes>${uwsgiconfig:processes}</processes>
  <master/>
  <disable-logging/>
</uwsgi>
